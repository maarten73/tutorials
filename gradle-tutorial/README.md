# Wat gaan we doen?

- Leren werken met gradle
- Een simpele Java applicatie builden vanop de command line (zonder IDE)

# Wat heb je nodig?

- 30 minuten tijd
- Een computer met MacOS
- Toegang tot internet
- Doorzettingsvermogen
- Basis kennis Java
- Optioneel:  IntelliJ IDEA
 

# Build Tools

## Wat is een build tool?

- Een build tool zet java code (en andere files) om naar een uitvoerbaar formaat
- De output is meestal een JAR file (= Java Archive file) of een WAR (= web archive)
- Een JAR-file is eigenlijk een ZIP-file = een (gecomprimeerde) verzameling van bestanden
- Java libraries worden gedistribueerd als JAR files

- javac = java compiler => zet  Wout.java om in Wout.class
- Build tool: 
  - Maakt .class files van alle .java files
  - Maakt een archive (jar) van alle .class files 


<p>
<details>
<summary>Klik hier om meer te lezen over de geschiedenis van build tools voor Java</summary>


# Geschiedenis van Build Tools voor Java

## Ant
* Sinds 2000, wordt nu niet meer vaak gebruikt
* Maakt gebruik van XML om het build proces te beschrijven

## Maven
* Eerste release in 2004
* Maakt gebruikt van 'convention over configuration' 
* Hierdoor krijgen alle projecten een gelijkaaridge folder layout => makkelijk voor developers
* Zeer populair
* Maakt ook gebruik van XML om het build proces te beschrijven (in een bestand genaamd pom.xml)

## Gradle
* Ontstaan in 2007
* Maakt gebuikr van de script-taal Groovy om build te beschrijven
* Hierdoor is de build-file (build.gradle) veel korter en leesbaarder

</details>
</p>

# Wat zijn de taken van een build tool?

* Zorgen voor 'repeatable builds'
  * Als ik het project bouw op mijn computer, dan wil ik hetzelfde resultaat krijgen als jij op jouw computer
* Beschrijft de afhankelijkheden (=dependencies) van je project   
* Het build proces is bij voorkeur onafhankelijk van de gebruikte IDE
  * Het maakt niet uit of je Eclipse of IntelliJ of Netbeans of ... gebruikt 
* Kijkt bij elke build na of je project nog werkt
  * door automatische testen uit te voeren
* Een build tool kan nog veel meer taken op zich nemen, zoals de output (een .jar bestand) van je project publiceren in een repository

# Repositories

* Er bestaan enorm veel open source java libraries en frameworks
* Deze worden meestal bewaard als JAR file in Maven repositories
* Een repository = een database van libraries
* Door een dependency toe te voegen aan je build file, kan jouw project gebruik maken van de library
 
### Bvb in maven 

```xml
<dependency>
   <groupId>joda-time</groupId>
   <artifactId>joda-time</artifactId>
   <version>2.10.1</version>
</dependency>
```

Bij het bouwen zal maven de overeenkomstige JAR downloaden van [https://repo1.maven.org/maven2/joda-time/joda-time/2.10.1/](https://repo1.maven.org/maven2/joda-time/joda-time/2.10.1/)

# Dependencies

Een dependency on maven (of gradle) heeft drie belangrijke coördinaten:
* groupId = meestal de organisatie die de library gemaakt heeft
* artifactId = de naam van de library
* version = de versie die je wil gebruiken

Aan de hand van deze 3 coördinaten weet gradle (of maven) exact welke library je wil gebruiken.

# Transitieve dependencies
Vaak zal de library die je wil gebruiken zelf ook dependencies hebben.
Gradle zal deze 'transitieve' dependencies ook automatisch downloaden voor jou.

Bijvoorbeeld
* project maakt gebruik van library A
* library A maakt gebruik van library B
* library B maakt gebruik van library C

gradle zal dan A, B en C downloaden omdat je project een dependency heeft op A  (idem voor maven)

# Locale cache van dependencies

Gradle en Maven gebruiken een lokale cache 
om te vermijden dat je alle dependencies bij elke build opnieuw moet downloaden

De files worden bewaard in ~/.m2/repository/   (~ = je Home folder)

Alle maven/gradle projecten op jouw computer maken gebruik van dezelfde lokale cache.
De eerste keer dat je het project bouwt:

'Downloading from central: https://repo.maven.apache.org/maven2/joda-time/joda-time/2.10.6/joda-time-2.10.6.jar'

Alle volgende keren: 
  download is niet nodig, gradle gebruikt het bestand in ~/.m2/repository/joda-time/joda-time/2.10.6/joda-time-2.10.6.jar

# Open een Terminal op MacOs

Om beter te begrijpen wat er gebeurt, gaan we in de volgende stappen werken vanop 'de command line' en dus niet met een IDE.

Om de **Terminal** applicatie te openen:

* ⌘ + spacebar   => hiermee open je Spotlight
* Type 'Terminal' + Enter
* Er zou een Terminal venster moeten openen
* Om binnen Terminal een nieuwe Tab te openen, kan je ⌘ + t gebruiken

Hier kan je zien hoe een Terminal venster er bij mij uit ziet: [Terminal window](images/Terminal.png)

# Enkele handige commando's

Deze commando's kan je gebruiken op de command-line:

| Commando | Beschrijving | Voorbeeld | Dus |
| ------ | ------ | ------ | ------ |
| pwd | print working directory | pwd | waar zijn we? |
| cd | change directory | cd helloworld |  ga naar de helloworld folder | 
| mkdir | maak een nieuwe directory | mkdir src | Maak in huidige folder een nieuwe folder met naam src |
| find  | find bestanden | find . |  vind alle bestanden in de huidige folder |
| touch | maak een bestand aan | touch xxx.txt | maak een leeg bestand aan met de naam xxx.txt |
| open  | open een bestand met de bijhorende applicatie | open -e xxx.txt | open het bestand xxx.txt met TextEdit |


Een grondige bespreking van deze commado's zit niet in de scope van deze tutorial.

Genoeg theorie, nu ga je zelf [aan de slag](Aan de slag.md)
  

 


