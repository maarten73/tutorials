# Nu gaan we echt aan de slag

Deze tutorial is gebaseerd op [https://spring.io/guides/gs/gradle/](https://spring.io/guides/gs/gradle/)

We gaan een eenvoudig Java project bouwen met gradle

Om te beginnen:  
* Open een Terminal venster
* Maak een directory aan waarin we het project gaan bewaren
* Maak binnen die directory de folder waarin we de Java klassen gaan bewaren

```bash
cd
mkdir helloworld
cd helloworld
mkdir -p src/main/java/hello
find .
```

![mkdir](./images/mkdir-hello.png "Make the project folder")


# Bestanden aanmaken en openen vanop de command-line

Omdat we in deze tutorial voorlopig geen IDE gebruiken, moeten we op een andere manier bestanden aanmaken en bewerken.

* Je kan een leeg bestand aanmaken met het commando 'touch'
* Je kan het bestand openen met 'open -e' 

Voer volgende commando's uit in een Terminal

```bash
cd
cd helloworld
touch src/main/java/hello/HelloWorld.java
touch src/main/java/hello/Greeter.java
open -e src/main/java/hello/HelloWorld.java src/main/java/hello/Greeter.java
```

Kopieer deze code naar Greeter.java
 
```java
package hello;

public class Greeter {

    public String sayHello() {
        return "Hello world!";
    }
}
```

Kopieer deze code naar HelloWorld.java

```java
package hello;

public class HelloWorld {

    public static void main(String[] args) {
        Greeter greeter = new Greeter();
        System.out.println(greeter.sayHello());
    }

}
```

# Gradle installeren

Nu is het tijd om gradle te installeren. Op MacOs zijn er meerdere opties om dit te doen
We kiezen voor sdkman (https://sdkman.io/install)

Voer volgende commando’s uit in een Terminal:

```bash
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh”
sdk version
sdk install gradle
```

Als dit gelukt is, kan je sdkman gebruiken om gradle te installeren:

```bash
sdkman gradle
gradle –v
```

Als alles goed is, krijg je de versie van gradle (en Ant en Java) te zien

# Uitvinden wat gradle kan doen

Voer volgende commando’s uit in een Terminal:

```bash
cd
cd helloworld
gradle tasks
touch build.gradle
open –e build.gradle
```

Kopieer volgende code naar het bestand *build.gradle*

```
apply plugin: 'java'
```

Je hebt nu een heel eenvoudig *build.gradle* bestand, maar het is wel heel krachtig.

Voer volgend commando uit in een Terminal

```bash
gradle tasks
```

Dankzij die ene lijn hierboven, weet gradle dat dit een Java project is. 
Hierdoor zijn er taken om het project te bouwen, testen uit te voeren, javadoc aan te maken en nog meer.

# Bouw je project

Nu is het eindelijk tijd om jouw project te bouwen. De *build* taak zal je vaak gebruiken. 
Deze taak zal 
* de code compileren (.java files omzetten in .class files)
* testen uitvoeren als die er zijn (voorlopig niet)
* de .class bestanden verzamelen in een .jar bestand

Voer volgend commando uit in een Terminal

```bash
gradle build
```

Na enkele seconden zal je zien dat “de build” gelukt is
Neem een kijkje in de build folder om het resultaat van het bouwen te zien.

```bash
find build
```

Je zal onder andere deze folders zien

| Folder | Beschrijving |
| ------ | ------ |
| classes | bevat de gecompileerde .class bestanden |
| reports | bevat rapporten van de build, zoals test reports |
| libs | libraries die het project nodig heeft (meestal JAR of WAR bestanden) |

De build folder hoef je niet te bewaren omdat de inhoud steeds opnieuw gebouwd kan worden
Je kan grote kuis houden met:

```bash
gradle clean
```

De build folder zal nu verdwenen zijn.

# Deel 2: dependencies toevoegen

Je hebt deel 1 van de tutorial nu afgewerkt. Proficiat.

Voor deel 2 kan je de code vinden in https://github.com/spring-guides/gs-gradle/tree/master/complete

Als je wil kan je nu ook gebruik maken van IntelliJ IDEA
* Open IDEA en sluit alle projecten die nog open staan
* Kies "Open or Import ..."
* Selecteer de folder waarin je de code bewaard hebt (helloworld)

IntelliJ zal *build.gradle* vinden en begrijpen dat dit een Java project is dat met gradle gebouwd moet worden en waarvan de klassen in *src/main/java* te vinden zijn.

![alt text](images/intellij.png "IntelliJ IDEA")


Momenteel gebruikt je project enkel standaard Java klassen en geen externe libraries (= bibliotheken).
De meeste Java projecten maken wel gebruik van (open source) libraries.

Laten we de applicatie aanpassen zodat de huidige datum en tijd ook afgeprint worden.
We kunnen dat natuurlijk doen met standaard Java klassen, maar deze keer gaan we hiervoor de JodaTime library gebruiken.

Pas `HelloWorld.java` aan als volgt:

```java
package hello;

// we maken gebruik van de klasse LocalTime uit de package org.joda.time
import org.joda.time.LocalTime;

public class HelloWorld {
  public static void main(String[] args) {

    // de volgende twee lijnen zijn nieuw
    LocalTime currentTime = new LocalTime();
    System.out.println("The current local time is: " + currentTime);

	Greeter greeter = new Greeter();
	System.out.println(greeter.sayHello());
  }
}
```

De klasse `HelloWorld` maakt nu gebruik van de klasse `LocalTime` uit de JodaTime bibliotheek om de huidige tijd op te vragen.

Als ju nu *gradle build* uitvoert, dan zal je een fout krijgen omdat de JodaTime niet hebt opgegeven als een dependency (= afhankelijkheid) van je project.

Dit ziet er zo uit: https://gitlab.com/maarten73/tutorials/-/blob/master/gradle-tutorial/images/build-failed.png

Voeg deze lijnen toe aan `build.gradle`

```gradle
repositories {
    mavenCentral()
}

dependencies {
    implementation "joda-time:joda-time:2.2"
}
```
En voer opnieuw *gradle build* uit in een Terminal venster.
De build zou nu moeten slagen.

Met bovenstaande aanpassing, zeggen we tegen gradle:
* zoek dependencies in de Maven Central repository. Dit is een populaire repository die enorm veel open source libaries bevat.
* gebruik versie 2.2 van de joda-time library 

# De applicatie opstaren

Voeg nu deze twee lijnen toe aan `build.gradle` om gradle te vertellen dat jouw project aan applicatie is.

```gradle
apply plugin: 'application'
mainClassName = 'hello.HelloWorld'
```

Hiermee zeggen we:
* dit project bevat een applicatie (een klasse met een public void main method)
* de main class van het project is `hello.HelloWorld`

Nu kan je de applicate starten vanuit een Terminal venster:

```bash
gradle run
```

De output zou er als volgt moeten uit zien:

```
> Task :run
The current local time is: 18:20:19.938
Hello world!

BUILD SUCCESSFUL in 763ms
2 actionable tasks: 1 executed, 1 up-to-date
```

Zo ziet het er bij mij uit:  https://gitlab.com/maarten73/tutorials/-/blob/master/gradle-tutorial/images/gradle-run.png

# Proficiat!!  

Je bent nu klaar met deze tutorial.

Je hebt enorm veel geleerd op korte tijd. 

Je weet nu

* wat de voordelen zijn van een build tool
* hoe je gradle kan installeren op je computer
* hoe je een nieuw project start
* hoe je dependencies toevoegt aan een gradle project
* hoe je een gradle project kan bouwen vanop de command line

Stuur een mailtje naar mailto:gradle@bosteels.eu om te zeggen wat je ervan vond en wat je nu graag zou willen leren.
